#include "imagemPGM.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

ImagemPGM::ImagemPGM(string tipo, int posicao_inicial, int altura, int largura, int nivel_max_cor){
  this-> tipo = tipo;
  this-> posicao_inicial = posicao_inicial;
  this-> altura = altura;
  this-> largura = largura;
  this-> nivel_max_cor = nivel_max_cor;
}
//Decifra a mensagem escondida na imagem no formato PGM
void ImagemPGM::decifra(string local_arquivo, int posicao_inicial){
  //Variáveis
  string linha;
  char byte;
  char bit;
  char caracter;
//Abertura do arquivo
  ifstream nome_arquivo;
  nome_arquivo.open(local_arquivo);

//Colocando o cursor no início da matriz de pixels
  for(int i=0; i<4; i++){
    getline(nome_arquivo, linha, '\n');
  }
//Colocando o cursor no inicio da mensagem
  nome_arquivo.seekg(posicao_inicial, ios_base::cur);
  cout << endl << "A mensagem é: ";
//Pegando o ultimo bit de cada byte, formando um caracter e printando na tela
  while(caracter != '#'){
    caracter = 0;
     for(int i=0; i<8; i++){
       nome_arquivo.get(byte);
       bit = byte & 0x01;
       if(i==0){
         caracter = caracter | (bit <<1);
       }else if(i==7){
         caracter = caracter | bit;
       }else{
         caracter = (caracter << 1) | (bit << 1);
       }
     }
    cout << caracter;
  }
  cout << endl;
  nome_arquivo.close();
}
