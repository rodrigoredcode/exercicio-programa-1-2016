#include "imagem.hpp"
#include "imagemPGM.hpp"
#include "imagemPPM.hpp"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main(int argc, char ** argv){

  Imagem * imagemGenerica = new Imagem();
  string caminho_arquivo;
  string caminho_novo_arquivo;
  char filtro;

  cout << endl << "~~~~~ Exercicio programa 1 ~~~~~" << endl;
  cout << endl << "Digite o caminho da imagem (PGM ou PPM) a ser decifrada: ";
  cin >> caminho_arquivo;

  //Lendo cabeçalho da imagem e armazenando num objeto genérico
  imagemGenerica->leImagem(caminho_arquivo);

  //Descobrindo de que tipo é, armazenando no objeto de mesmo tipo e chamando o método que decifra
  if(imagemGenerica->getTipo() == "P5"){
    ImagemPGM * imagempgm = new ImagemPGM(imagemGenerica->getTipo(), imagemGenerica->getPosicaoInicial(), imagemGenerica->getAltura(), imagemGenerica->getAltura(), imagemGenerica->getNivelMaxCor());

    delete(imagemGenerica);

    imagempgm->decifra(caminho_arquivo, imagempgm->getPosicaoInicial());
    exit(1);
  }else{
    cout << "Digite o caminho e o nome da nova imagem: ";
    cin >> caminho_novo_arquivo;
    cout << endl << "Digite o filtro desejado (R,G ou B): " << endl;
    cin >> filtro;

    ImagemPPM* imagemppm = new ImagemPPM(imagemGenerica->getTipo(), imagemGenerica->getAltura(), imagemGenerica->getAltura(), imagemGenerica->getNivelMaxCor());

    delete(imagemGenerica);

    imagemppm->decifra(caminho_arquivo, caminho_novo_arquivo, imagemppm, filtro);
    exit(1);
  }

}
