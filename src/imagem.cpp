#include "imagem.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

//Construtor Padrão
Imagem::Imagem(){

}
//Acessores
string Imagem::getTipo(){
  return tipo;
}
void Imagem::setTipo(string tipo){
  this-> tipo = tipo;
}
int Imagem::getPosicaoInicial(){
  return posicao_inicial;
}

void Imagem::setPosicaoInicial(int posicao_inicial){
  this-> posicao_inicial = posicao_inicial;
}

int Imagem::getLargura(){
  return largura;
}
void Imagem::setLargura(int largura){
  this-> largura = largura;
}
int Imagem::getAltura(){
  return altura;
}
void Imagem::setAltura(int altura){
  this-> altura = altura;
}
int Imagem::getNivelMaxCor(){
  return nivel_max_cor;
}
void Imagem::setNivelMaxCor(int nivel_max_cor){
  this-> nivel_max_cor = nivel_max_cor;
}
//Outros métodos

//Tenta abrir a imagem, verifica se foi aberta e adiciona o cabeçalho nos atributos do objeto
void Imagem::leImagem(string caminho_arquivo){
  string tipo;
  string comentario;
  char hash;
  char traco;
  int posicao_inicial = 0;
  int altura = 0;
  int largura = 0;
  int nivel_max_cor = 0;

  //Abrindo arquivo
  ifstream nome_arquivo;
  nome_arquivo.open(caminho_arquivo, ios_base::binary);

  //Verifica se o arquivo foi aberto
  if(nome_arquivo.is_open()){
    cout << endl << "---- Arquivo aberto com sucesso! ----" << endl;
  }else{
    cout << "---- Erro ao tentar abrir o arquivo! ----" << endl;
    exit(1);
  }
  //Verifica que tipo de imagem é e adiciona as informações do cabeçalho nos atributos
  nome_arquivo >> tipo;
  if(tipo == "P5"){
    nome_arquivo >> hash >> posicao_inicial >> traco >> comentario >> comentario >> comentario >> comentario >> altura >> largura >> nivel_max_cor;
  }else if(tipo == "P6"){
    nome_arquivo >> comentario >> altura >> largura >> nivel_max_cor;
  }else{
    cerr << "Tipo de imagem inválido, escolha uma imagem do tipo PPM ou PGM" << endl;
  }
  this-> posicao_inicial = posicao_inicial;
  this-> tipo = tipo;
  this-> altura = altura;
  this-> largura = largura;
  this-> nivel_max_cor = nivel_max_cor;

  //Fecha o arquivo

  nome_arquivo.close();
}
