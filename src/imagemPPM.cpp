#include "imagemPPM.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

ImagemPPM::ImagemPPM(string tipo, int altura, int largura, int nivel_max_cor){
  this-> tipo = tipo;
  this-> altura = altura;
  this-> largura = largura;
  this-> nivel_max_cor = nivel_max_cor;
}

void ImagemPPM::decifra(string local_arquivo, string local_novo_arquivo, ImagemPPM * imagemppm, char filtro){
  string linha;
  string comentario;
  char red;
  char blue = 255;
  char green = 255;

//Abre a imagem original no modo leitura
  ifstream nome_arquivo;
  nome_arquivo.open(local_arquivo, ios_base::binary);

//Posiciona o cursor no início da matriz de pixels
  for(int i=0; i<4; i++){
      getline(nome_arquivo, linha, '\n');
      if(i==1){
        comentario = linha;
      }
  }

//Cria uma nova imagem
  ofstream nome_novo_arquivo;
  nome_novo_arquivo.open(local_novo_arquivo, ios_base::binary);

//Escreve o cabeçalho na nova imagem
  nome_novo_arquivo << imagemppm->getTipo() << endl << comentario << endl << imagemppm->getAltura() << ' ' << imagemppm->getLargura() << endl << imagemppm->getNivelMaxCor() << endl;
  
//Verifica e aplica o filtro escolhido
if(filtro == 'R'){
  for(int i=0; i<imagemppm->getAltura(); i++){
    for(int j=0; j<imagemppm->getLargura(); j++){
      nome_arquivo.get(red);
      nome_arquivo.get(green);
      nome_arquivo.get(blue);
      blue = 255;
      green = 255;
      nome_novo_arquivo << red << green << blue;
    }
  }
cout << "Filtro 'R' aplicado ! Verifique se a imagem foi decifrada" << endl;
}else if(filtro == 'G'){
  for(int i=0; i<imagemppm->getAltura(); i++){
    for(int j=0; j<imagemppm->getLargura(); j++){
      nome_arquivo.get(red);
      nome_arquivo.get(green);
      nome_arquivo.get(blue);
      red = 255;
      blue = 255;
      nome_novo_arquivo << red << green << blue;
    }
  }
cout << "Filtro 'G' aplicado ! Verifique se a imagem foi decifrada" << endl;
}else if(filtro == 'B'){
  for(int i=0; i<imagemppm->getAltura(); i++){
    for(int j=0; j<imagemppm->getLargura(); j++){
      nome_arquivo.get(red);
      nome_arquivo.get(green);
      nome_arquivo.get(blue);
      red = 255;
      green = 255;
      nome_novo_arquivo << red << green << blue;
    }
  }
cout << "Filtro 'B' aplicado ! Verifique se a imagem foi decifrada" << endl;
}else{
  cout << "Tipo de filtro incorreto !";
}
nome_arquivo.close();
nome_novo_arquivo.close();
}
