#ifndef IMAGEMPGM_HPP
#define IMAGEMPGM_HPP

#include "imagem.hpp"
#include <iostream>
#include <fstream>

using namespace std;

class ImagemPGM : public Imagem{
  public:
    ImagemPGM(string tipo, int posicao_inicial, int altura, int largura, int nivel_max_cor);
    void decifra(string local_arquivo, int posicao_inicial);
};
#endif
