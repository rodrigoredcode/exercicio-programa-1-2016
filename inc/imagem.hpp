#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <string>
#include <iostream>
#include <fstream>

using namespace std;

class Imagem{
	//Atributos
protected:
		string tipo;
		int largura;
		int altura;
		int nivel_max_cor;
		int posicao_inicial;
	//Métodos
	public:
		Imagem();

		string getTipo();
		void setTipo(string tipo);

		int getLargura();
		void setLargura(int largura);

		int getAltura();
		void setAltura(int altura);

		int getNivelMaxCor();
		void setNivelMaxCor(int nivel_max_cor);

		int getPosicaoInicial();
		void setPosicaoInicial(int posicao_inicial);

		void leImagem(string caminho_arquivo);

};
#endif
