#ifndef IMAGEMPPM_HPP
#define IMAGEMPPM_HPP

#include "imagem.hpp"
#include <iostream>
#include <fstream>
#include <string>

class ImagemPPM : public Imagem{
public:
  ImagemPPM(string tipo, int altura, int largura, int nivel_max_cor);

  void decifra(string local_arquivo, string local_novo_arquivo, ImagemPPM * imagemppm, char filtro);
};
#endif
